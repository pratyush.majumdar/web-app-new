# Project Title

Sample Web App

## Getting Started

A sample Web App in Java to be used to test any deployment on Java Web Containers. It has a index.jsp page and a [REST API EndPoint]
(http://localhost:8080/rest/service/hello)