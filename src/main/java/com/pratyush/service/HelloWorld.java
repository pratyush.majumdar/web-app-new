package com.pratyush.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/service")
public class HelloWorld {
	
	@GET
	@Path("/hello")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee sayHello() {
		Employee employee = new Employee("Pratyush", 30);
		return employee;
	}
	
	public class Employee {
		private String name;
		private int age;
		
		public Employee(String name, int age) {
			this.name = name;
			this.age = age;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}
	}
}
